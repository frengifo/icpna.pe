<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'icpna_web');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '~VB)x3NY}AZ_bnI4]%>QN5T:2+S.sj-3_1dXhP>0s}}W{2>fVLOE:nxENuh^niZx');
define('SECURE_AUTH_KEY',  'b5kngsAS|![?>[tj,q7_<f6>ALq;(xy]=tE8]lazUE)jNpGkX{i0BO/a-aWN#*9#');
define('LOGGED_IN_KEY',    'ZhH/!+f=kXMr$IR@j|@e&k?.4$kv{ka;wq6_sbB@L:)7++r;-)/yI)UP@FKA!ObZ');
define('NONCE_KEY',        'UcM6(DLf+jvh&]^k.*3[[*x|!6E;+kW7Bzl%/{K+r1iAm`HeVgSES.-tsNJOq3J]');
define('AUTH_SALT',        '_T17;efO;,<N&)oO:[z=S^24VE1 zkhhu3[m7<vIHn}9-Hcdo}u/lV8i9}IVCzSv');
define('SECURE_AUTH_SALT', 'H80$i9_u]i{cdD*C,+skP-q#3ZJE;*+gJ%#h0K8Cn_|i+etMDO_ICN~hV 3ZK/bg');
define('LOGGED_IN_SALT',   ')<&pwqE4$,d9+iA$loj@mx)|L;$.Yth4Nk-%I{*Fy|nT#67GLo-J{+R%^]Y_4TB_');
define('NONCE_SALT',       ':=_/+/17g:%bSQ6E:6k0)i+hwj@6AAe?Tas[~]^woFg!2-v79Uw[8,I4%a.p?7q)');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
