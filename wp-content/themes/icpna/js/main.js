$(document).ready(function(){
		
		$('.banner').unslider({
			speed: 500,               //  The speed to animate each slide (in milliseconds)
			delay: 3000,              //  The delay between slide animations (in milliseconds)
			complete: function() {},  //  A function that gets called after every slide animation
			keys: true,               //  Enable keyboard (left, right) arrow shortcuts
			dots: true,               //  Display dot navigation
			fluid: true 
		});
		$( ".btn-menu" ).click(function() {
		  $( ".menu-menu-principal-container" ).slideToggle();
		});
		

});

var config = {
             
              viewport: window.document.documentElement,
              vFactor: 0.80,
              mobile: true
            }
window.sr = new scrollReveal(config);