		
		<footer>
			<div class="Wrapper color-white">
				<div class="left" >
					<a href="" >
						<img src="<?=get_template_directory_uri(); ?>/img/logo-white.png">
					</a>
					<p>
						(511) 706 700<br>
						<b><a href="" class="color-white">wwww.icpna.edu.pe</a></b>
					</p>
					<p>
						<a href="">
							<img src="<?=get_template_directory_uri(); ?>/img/fb.jpg"> bibliotescaicpna
						</a> <br>
						<a href="">
							<img src="<?=get_template_directory_uri(); ?>/img/fb.jpg"> icpnaoficial
						</a>
					</p>	
				</div>
				<ul class="sedes-footer">
					<li><b>Biblioteca Luis E. Varcárcel</b> - Sede Lima Centro - bibliotecalima@icpna.edu.pe - anexo 1272</li>
					<li><b>Biblioteca Estuardo Núñez</b> - Sede Miraflores - bibliotecamiraflores@icpna.edu.pe - anexo 2272</li>
					<li><b>Biblioteca Jorge Basadre Grohmann</b> - Sede La Molina - bibliotecalm@icpna.edu.pe - anexo 4272</li>
					<li><b>Biblioteca ICPNA Lima Norte</b> - Sede Lima Norte - bibliotecaln@icpna.edu.pe - anexo 5272</li>
					<li><b>Biblioteca ICPNA Chimbote</b> -  informeschimbote@icpna.edu.pe - anexo *02170</li>
					<li><b>Biblioteca ICPNA Iquitos</b> -  informesiquitos@icpna.edu.pe - anexo *011701</li>

					
				</ul>
				<div class="right">
					<img src="<?=get_template_directory_uri(); ?>/img/1.png">
					<img src="<?=get_template_directory_uri(); ?>/img/2.png">
					<img src="<?=get_template_directory_uri(); ?>/img/3.png">
				</div>

			</div>
			<div class="bottom-footer text-center color-white">
				<p>COPYRIGHT 2015. Todos los derechos reservados</p>
			</div>
		</footer>


	</div>

	<!-- Placed at the end of the document so the pages load faster -->
	<script>
		window.jQuery || document.write('<script src="<?=get_template_directory_uri(); ?>/js/jquery.min.js"><\/script>')
	</script>
	<script src="<?=get_template_directory_uri(); ?>/js/plugins.js"></script>
	<script src="<?=get_template_directory_uri(); ?>/js/main.js?v=14"></script>
</body>
</html>