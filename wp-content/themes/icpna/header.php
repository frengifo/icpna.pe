<html>
<head>
	<title><?=the_title(); ?></title>

	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge;" />
	<meta name="viewport" id="viewport" content="width=device-width, height=device-height, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="description" content="" />
	<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700|Roboto:300,400,700' rel='stylesheet' type='text/css'>
	<link type="text/css" rel="stylesheet" href="<?=get_template_directory_uri(); ?>/css/bootstrap.min.css" />
	<link type="text/css" rel="stylesheet" href="<?=get_template_directory_uri(); ?>/css/main.css" />
	<script src="<?=get_template_directory_uri(); ?>/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>
<body>



	
	<header class="" >
		<div class="top-menu text-right">
			<div class="Wrapper">
				<div class="left">ICPNA | Centro cultural</div>
				<ul>
					<li>
						<a href="#">Catálogo en línea</a>
					</li>
					<li>
						<a href="#">Biblioteca virtual</a>
					</li>
					<li>
						<a href="#">Preguntas frecuentes</a>
					</li>
				</ul>
			</div>
		</div>
			<?php 

				$defaults = array(
					
					'menu'            => 'menu-principal',
					'container'       => 'nav',
					'menu_class'      => 'menu',
					'echo'            => true,
					'items_wrap'      => '<ul id="%1$s" class="%2$s" >%3$s</ul>',
					'walker'          => ''
				);


			 ?>
		<div class="Wrapper">
			<div class="logo" data-sr="reset, enter bottom, roll 45deg, over 0.5s">
				<div class="btn-menu"></div>
				<a href="/" title="Inicio Icpna" >
					<img src="<?=get_template_directory_uri(); ?>/img/logo.png">
				</a>
			</div>
			<div class="redes" >
				<span data-sr>
					<a href="#" title="Facebook Icpna">
						<img src="<?=get_template_directory_uri(); ?>/img/fb.jpg">
					</a>
				</span>
				<span data-sr>
					<a href="#" title="Twitter Icpna">
						<img src="<?=get_template_directory_uri(); ?>/img/tw.jpg">
					</a>
				</span>
			</div>
			<?php wp_nav_menu( $defaults ); ?>
		</div>
	</header>

	
	<div id="main">

	