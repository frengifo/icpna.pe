<?php
/*
Template Name: Search Page
*/
?>
<?php get_header(); ?>
	

		<?php $header_image  = trim($header_image) != '' ? '':'background-image:url("'.$header_image.'");' ?>
			
		<section class="title-page" style="">

			<div class="Wrapper">
				<h2 class="left heading-page">Busqueda de noticias: "<?php echo $_GET['s']; ?>" <br> <small><?php echo count($post); ?></small>		</h2>
				<?php var_dump($post); ?>
				<?php $mySearch =& new WP_Query("s=$s & showposts=-1");
					$num = $mySearch->post_count; var_dump($num); ?>
				<ul class="breadcrumb text-right right">
	              <li>
	                <a href="/">Inicio</a>
	              </li>
	              
	              <li class="active">Noticias</li>
	            </ul>
			</div>
		</section>
		<div class="row Wrapper ">
			
			<div class="col-md-8 nopadding noticias-home ">

				<section class="ultimas-noticias " data-sr>
					<div class="row">

						<?php while ( have_posts() ) : the_post(); ?>
							
							<article class="col-md-4 col-sm-6 col-xs-6" >
								<?php $noticiaImage = $dynamic_featured_image->get_featured_images($post->ID);?>
								<a href="/noticias/<?php echo $post->post_name; ?>" class="image-noticia" style="background-image:url('<?php echo $noticiaImage[0]['full']; ?>');">
									
									
								</a>
								
								<p class="fecha-noticia"><small><?php  echo date('d',strtotime($post->post_date))."".fechaEsp($post->post_date); ?></small></p>
								<h3><a href="/noticias/<?php echo $post->post_name; ?>"><?php echo $post->post_title; ?></a></h3>
								<p><?php echo $post->post_excerpt; ?></p>
							</article>

						<?php endwhile; ?>
					</div>
				</section>
			</div>
			<div class="col-md-4 nopadding aside-left">
				
				<form class="form-boletin">

					<h3>¡Inscribete a nuestro boletín Mensual!</h3>
					<div class="control-form">
						<input type="text" name="nombre" placeholder="Nombre">
						<input type="email" name="email" placeholder ="Correo Electrónico">
						<label for="check-terminos">
							<input type="checkbox" name="terminos" value="1" id="check-terminos">
							Aceptar términos de privacidad de datos
						</label>
						<input type="submit" value="Registrarme" class="btnRegistrar color-white">

					</div>
					
				</form>

				<?php get_search_form(); ?>
			</div>
		</div>
		

	
<?php get_footer(); ?>