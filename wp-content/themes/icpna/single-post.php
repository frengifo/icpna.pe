<!--Registra las visitas-->
<?php wpb_set_post_views(get_the_ID()); ?>
<?php get_header(); ?>
	<?php $customHeader =& get_children( 'post_type=attachment&post_mime_type=image&post_parent=17' ); ?>
	<?php foreach ($customHeader as $key): ?>
		<?php $header_image = $key->guid; ?>
	<?php endforeach ?>
	<section class="title-page" style="background-image:url('<?php echo $header_image; ?>');">

		<div class="Wrapper">
			<h2 class="left heading-page">Noticias</h2>
			<ul class="breadcrumb text-right right">
              <li>
                <a href="/">Inicio</a>
              </li>
              
              <li class="active">Noticias</li>
            </ul>
		</div>
	</section>
	<div class="row Wrapper ">
		
		<div class="col-md-8 nopadding noticias-home noticias-interna">

			<?php while ( have_posts() ) : the_post(); ?>
			<?php $featuredImage = $dynamic_featured_image->get_featured_images(get_the_id())[0]['full']; ?>
			<div class="interna-content-noticia">
				<article>
					<h1><?php the_title(); ?></h1>
					<img src="<?php echo $featuredImage; ?>" class="image-principal-noticia">
					<?php  the_content(); ?>
				</article>
			</div>

			<?php endwhile; ?>

			<section class=" articulos-destacados">
				<h2>Articulos destacados</h2>
				<?php 
				$popularpost = new WP_Query( array( 'posts_per_page' => 3, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC'  ) );
					while ( $popularpost->have_posts() ) : $popularpost->the_post();

						$featuredImage = $dynamic_featured_image->get_featured_images(get_the_id())[0]['thumb'];
					?>

					<article>
						<a href="/noticias/<?php echo $post->post_name ?>" style="background-image:url('<?php echo $featuredImage; ?>')" class="image-thumb">
							&nbsp;
						</a>
						<h5><a href="/noticias/<?php echo $post->post_name ?>"><?php echo $post->post_title; ?></a></h5>
					</article>
					<?php
					endwhile;
				?>
			</section>
		</div>
		<div class="col-md-4 nopadding aside-left">
			<form class="form-boletin">

				<h3>¡Inscribete a nuestro boletín Mensual!</h3>
				<div class="control-form">
					<input type="text" name="nombre" placeholder="Nombre">
					<input type="email" name="email" placeholder ="Correo Electrónico">
					<label for="check-terminos">
						<input type="checkbox" name="terminos" value="1" id="check-terminos">
						Aceptar términos de privacidad de datos
					</label>
					<input type="submit" value="Registrarme" class="btnRegistrar color-white">

				</div>
				
			</form>

			<?php get_search_form(); ?>
		</div>
	</div>
<?php get_footer(); ?>