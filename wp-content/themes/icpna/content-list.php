<div class="col-md-4 ">
	<section class="list-produtcs bg-white row">
		<div class="col-md-12 bg-white"><h4>Línea de productos</h4></div>
		<?php //wp_list_pages( array('depth' => 1) ); ?>

		<?php foreach ($pagesCategorias as $key): ?>
			<section class="col-md-12 bg-white">
				<h5><a href="<?=get_page_uri( $key );?>"><?php echo $key->post_title; ?></a></h5>
				<?php $pagesLinea = get_pages( array( 'parent' => $key->ID , 'hierarchical' => 0, 'sort_column' => 'menu_order' ) ) ?>
				
				<?php $stepCol = ceil(count($pagesLinea)/2); ?>
				<?php $n_col = ($stepCol < 3) ? "12":"6"; ?>
				<?php if ($n_col == "6"): ?>
						
						<?php $i=0; ?>
						
							<?php foreach ($pagesLinea as $val): ?>
								
								<?php if ($i==0): ?>
									<ul class="col-md-<?=$n_col?>">
								<?php endif ?>
										
										<li><a href="<?=get_page_uri( $val->ID );?>"><?=$val->post_title?></a></li>	
								
								<?php $i++; ?>

								<?php if ($i==$stepCol): ?>
									</ul>
									<?php $i=0; ?>
								<?php endif ?>
								
							<?php endforeach ?>

				<?php endif ?>
				
				<?php if ($n_col == "12"): ?>
						<ul class="col-md-<?=$n_col?>">
						<?php foreach ($pagesLinea as $val): ?>
								<li><a href="<?=get_page_uri( $val->ID );?>"><?=$val->post_title?></a></li>	
						<?php endforeach ?>
						</ul>																		
				<?php endif ?>

			</section>
		<?php endforeach ?>
		
		
	</section>
</div>