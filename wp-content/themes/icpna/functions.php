<?php
add_action( 'after_setup_theme', 'register_my_menu' );
function register_my_menu() {
  register_nav_menu( 'primary', 'Primary Menu' );
}

function my_search_form( $form ) {
	$form = '
		<form role="search" method="get" id="searchform" class="form-inline nopadding text-center form-search" action="' . home_url( '/' ) . '" >

			<div class="box-search"><input type="text" value="' . get_search_query() . '" class="form-control txt-search" placeholder="Buscar noticias..." name="s"  /><input type="submit" value="&nbsp;" class="btn-search"></div>


		</form>
	';

	return $form;
}

add_filter( 'get_search_form', 'my_search_form' );


if ( function_exists('register_sidebar') )
    register_sidebar( array(
   'name' => __( 'Productos destacados'),
   'id' => 'productos_destacados',
   'description' => __( ' ', 'viennatone' ),
   'before_widget' => '<aside id="%1$s" class="widget %2$s">',
   'after_widget' => "</aside>",
   'before_title' => '<h3 class="widget-title">',
   'after_title' => '</h3>',
   ) );

add_filter('next_posts_link_attributes', 'posts_link_attributes_1');
add_filter('previous_posts_link_attributes', 'posts_link_attributes_2');

function fechaEsp($fecha){
  $dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
  $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
  
  return " de ".$meses[date('n',strtotime($fecha))-1]. " del ".date('Y') ;
}

function posts_link_attributes_1() {
    return 'class="prev-post"';
}
function posts_link_attributes_2() {
    return 'class="next-post"';
}
//add_theme_support( 'custom-header' );

function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

function wpb_get_post_views($postID){
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}



?>


