<?php
/*
Template Name: Recursos
*/


?>
<?php get_header(); ?>
	
	<div class="Wrapper " >
		
		<?php while ( have_posts() ) : the_post(); ?>
			
			<?php the_content(); ?>

		<?php endwhile; ?>
	</div>
<?php get_footer(); ?>