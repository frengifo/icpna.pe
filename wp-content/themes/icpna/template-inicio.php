<?php
/*
Template Name: Inicio
*/


?>
<?php get_header(); ?>
	
	<div class="Wrapper home-content" >

		<?php while ( have_posts() ) : the_post(); ?>
			

			<?php 

				function wp_get_attachment( $attachment_id ) {

					$attachment = get_post( $attachment_id );
					return array(
					    'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
					    'caption' => $attachment->post_excerpt,
					    'description' => $attachment->post_content,
					    'href' => get_permalink( $attachment->ID ),
					    'src' => $attachment->guid,
					    'title' => $attachment->post_title
					);
				}
			 ?>

			
			<div class="banner">
			 	<ul>
					<?php $featuredImage = $dynamic_featured_image->get_featured_images(get_the_id());?>
					<?php foreach ($featuredImage as $key): ?>

						<?php $link_to_image = get_post_meta( $key['attachment_id'], "_dfi_link_to_image", true ); ?>
						<?php $description = wp_get_attachment( $key['attachment_id'] )['description']; ?>
						
							<li style="background-image:url('<?php echo $key['full']; ?>');" class="banner-image">	
								
								<div class="banner-desc">
									<?php echo $description; ?>
								</div>
								<a href="<?php echo $meta_values; ?>" class="banner-link" title="<?php echo get_the_title( $key['attachment_id'] ); ?>" target="_blank">
									Leer más
								</a>
								

							</li>
							<li style="background-image:url('<?php echo $key['full']; ?>');" class="banner-image">	
								
								<div class="banner-desc">
									<?php echo $description; ?>
								</div>
								<a href="<?php echo $meta_values; ?>" class="banner-link" title="<?php echo get_the_title( $key['attachment_id'] ); ?>" target="_blank">
									Leer más
								</a>
								

							</li>

					<?php endforeach ?>
				</ul>
			</div>
		<?php endwhile; ?>
		
			<div class="row Wrapper ">
				<div class="col-md-8 nopadding noticias-home">
					<div class="btn-home" >
						<div class="row">
							<div class="col-md-4 col-sm-4  col-xs-6" data-sr="wait 0.5s, enter bottom, roll 45deg, over 0.5s">
								<a href="" >
									<span class="btn-icon">
										<span class="icon-left icon-personas"></span>
										<span class="icon-right icon-personas"></span>
									</span>
									<span class="btn-text">¡Hazte Socio!</span>
								</a>
							</div>	
							<div class="col-md-4 col-sm-4 col-xs-6" data-sr="wait 0.8s, enter bottom, roll 45deg, over 0.5s">
								<a href="" >
									<span class="btn-icon">
										<span class="icon-left icon-libro"></span>
										<span class="icon-right icon-libro"></span>
									</span>
									<span class="btn-text">Catálogo en línea</span>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-6" data-sr="wait 1.1s, enter bottom, roll 45deg, over 0.5s">
								<a href="">
									<span class="btn-icon">
										<span class="icon-left icon-libros"></span>
										<span class="icon-right icon-libros"></span>
									</span>
									<span class="btn-text">Biblioteca virtual</span>
								</a>
							</div>
						</div>
					</div>
					<h2 class="title-home-noticia-destacada">Noticias destacadas</h2>
					<section class="ultimas-noticias " data-sr>
						<div class="row">

						<?php $lastNews = get_posts(array('category_name'  => 'noticias', 'posts_per_page'   => 6 )); ?>
						<?php foreach ( $lastNews as $key): ?>
							
							

							<article class="col-md-4 col-sm-6 col-xs-6" >
								<?php $noticiaImage = $dynamic_featured_image->get_featured_images($key->ID);?>
								<a href="noticias/<?php echo $key->post_name; ?>" class="image-noticia" style="background-image:url('<?php echo $noticiaImage[0]['full']; ?>');">
									
									
								</a>
								
								<p class="fecha-noticia"><small><?php  echo date('d',strtotime($key->post_date))."".fechaEsp($key->post_date); ?></small></p>
								<h3><a href="noticias/<?php echo $key->post_name; ?>"><?php echo $key->post_title; ?></a></h3>
								<p><?php echo $key->post_excerpt; ?></p>
							</article>


						<?php endforeach ?>
						
						</div>
					</section>
				</div>
				<div class="col-md-4 nopadding aside-left">
					<form class="form-boletin">

						<h3>¡Inscribete a nuestro boletín Mensual!</h3>
						<div class="control-form">
							<input type="text" name="nombre" placeholder="Nombre">
							<input type="email" name="email" placeholder ="Correo Electrónico">
							<label for="check-terminos">
								<input type="checkbox" name="terminos" value="1" id="check-terminos">
								Aceptar términos de privacidad de datos
							</label>
							<input type="submit" value="Registrarme" class="btnRegistrar color-white">

						</div>
						
					</form>

					<?php get_search_form(); ?>
				</div>
			</div>
	</div>
<?php get_footer(); ?>