<?php
/*
Template Name: Noticias
*/


?>
<?php get_header(); ?>
	<?php $customHeader =& get_children( 'post_type=attachment&post_mime_type=image&post_parent=17' ); ?>
	<?php foreach ($customHeader as $key): ?>
		<?php $header_image = $key->guid; ?>
	<?php endforeach ?>
	<section class="title-page" style="background-image:url('<?php echo $header_image; ?>');">

		<div class="Wrapper">
			<h2 class="left heading-page"><?php echo the_title(); ?></h2>
			<ul class="breadcrumb text-right right">
              <li>
                <a href="/">Inicio</a>
              </li>
              
              <li class="active">Noticias</li>
            </ul>
		</div>
	</section>
	<div class="row Wrapper ">
		
		<div class="col-md-8 nopadding noticias-home ">

			<section class="ultimas-noticias " data-sr>
				<div class="row">

				<?php $lastNews = get_posts(array('category_name'  => 'noticias', 'posts_per_page'   => 6 )); ?>
				<?php foreach ( $lastNews as $key): ?>
					
					

					<article class="col-md-4 col-sm-6 col-xs-6" >
						<?php $noticiaImage = $dynamic_featured_image->get_featured_images($key->ID);?>
						<a href="/noticias/<?php echo $key->post_name; ?>" class="image-noticia" style="background-image:url('<?php echo $noticiaImage[0]['full']; ?>');">
							
							
						</a>
						
						<p class="fecha-noticia"><small><?php  echo date('d',strtotime($key->post_date))."".fechaEsp($key->post_date); ?></small></p>
						<h3><a href="/noticias/<?php echo $key->post_name; ?>"><?php echo $key->post_title; ?></a></h3>
						<p><?php echo $key->post_excerpt; ?></p>
					</article>


				<?php endforeach ?>
				
				</div>
			</section>
		</div>
		<div class="col-md-4 nopadding aside-left">

			<form class="form-boletin">

				<h3>¡Inscribete a nuestro boletín Mensual!</h3>
				<div class="control-form">
					<input type="text" name="nombre" placeholder="Nombre">
					<input type="email" name="email" placeholder ="Correo Electrónico">
					<label for="check-terminos">
						<input type="checkbox" name="terminos" value="1" id="check-terminos">
						Aceptar términos de privacidad de datos
					</label>
					<input type="submit" value="Registrarme" class="btnRegistrar color-white">

				</div>
				
			</form>
			
			<?php get_search_form(); ?>
		</div>
	</div>
<?php get_footer(); ?>