<?php
/*
Template Name: Publicaciones
*/


?>
<?php get_header(); ?>
	
	<div class="Wrapper " >
		<?php $args = array(
					'sort_order' => 'asc',
					'sort_column' => 'post_title',
					'hierarchical' => 1,
					'exclude' => '',
					'include' => '',
					'meta_key' => '',
					'meta_value' => '',
					'authors' => '',
					'child_of' => get_the_ID(),
					'parent' => -1,
					'exclude_tree' => '',
					'number' => '',
					'offset' => 0,
					'post_type' => 'page',
					'post_status' => 'publish'
				); 
	$pages = get_pages(array('child_of' => get_the_ID() )); 

	?>

		<?php foreach ($pages as $key): ?>
				<a href="/publicaciones/<?php echo get_the_category( $key->ID )[0]->category_nicename; ?>/<?php echo $key->post_name; ?>">
				<?php echo $key->post_title; ?>
				</a><br>
		<?php endforeach ?>
	</div>
<?php get_footer(); ?>