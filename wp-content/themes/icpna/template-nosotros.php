<?php
/*
Template Name: Nosotros
*/


?>
<?php get_header(); ?>
	
	<section class="title-page" style="background-image:url('<?php header_image() ?>');">

		<div class="Wrapper">
			<h2 class="left heading-page"><?php echo the_title(); ?></h2>
			<ul class="breadcrumb text-right right">
              <li>
                <a href="/">Inicio</a>
              </li>
              
              <li class="active">Nosotros</li>
            </ul>
		</div>
	</section>
	<div class="row Wrapper ">
		
		<div class="col-md-8 nopadding noticias-home ">

			<section class="ultimas-noticias " >
				<div class="row">
					<div class="col-md-12 content-page">
						
						<?php while ( have_posts() ) : the_post(); ?>
			
							<?php echo the_content(); ?>

						<?php endwhile; ?>
					</div>
				
				</div>
			</section>
		</div>
		<div class="col-md-4 nopadding aside-left">

			<form class="form-boletin">

				<h3>¡Inscribete a nuestro boletín Mensual!</h3>
				<div class="control-form">
					<input type="text" name="nombre" placeholder="Nombre">
					<input type="email" name="email" placeholder ="Correo Electrónico">
					<label for="check-terminos">
						<input type="checkbox" name="terminos" value="1" id="check-terminos">
						Aceptar términos de privacidad de datos
					</label>
					<input type="submit" value="Registrarme" class="btnRegistrar color-white">

				</div>
				
			</form>
			
			<?php get_search_form(); ?>
		</div>
	</div>
<?php get_footer(); ?>